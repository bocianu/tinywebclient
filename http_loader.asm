;; fujinet web loader routine
LOADER_ADDRESS = $0400;
TCP_TIMEOUT = $0f;

_R = $40;  // Read - predefined values for data transfer direction (to be set in DSTATS)
_W = $80;  // Write
_NO = $00; // No data transfer

METHOD_GET = 4;             // http method
METHOD_PROPFIND = 6;        // to be set in AUX1 on Open
METHOD_PUT = 8;
METHOD_GET_WITH_HEADERS = 12;
METHOD_POST = 13;

CR2EOL = 1;                 // EOL auto translation
LF2EOL = 2;                 // to be set in AUX2 on Open
CRLF2EOL = 3;

MODE_BODY = 0;              // HTTP Channel Mode
MODE_COLLECT_HEADERS = 1;   // to be set in AUX2 on Command "M" - Set Channel Mode
MODE_GET_HEADERS = 2;
MODE_SET_HEADERS = 3;
MODE_SET_POST_DATA = 4;

JSIOINT = $E459; // SIO OS procedure vector
DCB = $300;      // DCB struct address

    org LOADER_ADDRESS
   
loader
                        ;; init 
    lda #0  
    sta shouldRun+1
                        ;; open HTTP connection
    lda #<openConnectionDCB
    jsr loadDCB_execSIO
                        ;; exit on error
    cpy #1
    jne error

waitForData
    jsr checkForData    ;; read status and wait for data
    beq waitForData
                        
    lda DCB_blockStart  ;; if DCB_blockStart specified read raw data block
    ora DCB_blockStart + 1
    beq blockLoop       ;; if DCB_blockStart = 0 -> no vector specified, read block header

oneBlockOnly
    lda status 
    sta DCB_blockLen
    sta DCB_blockLen + 2
    lda status + 1
    sta DCB_blockLen + 1
    sta DCB_blockLen + 3
    jmp getBlock            ;; and skip header reading

blockLoop
    lda #<readHeaderDCB
    jsr loadDCB_execSIO   ;; read first word
    cpw #$FFFF tmp_address  ;; if $ffff (com header)
    beq blockLoop ;; drop and read again 
    mwa tmp_address DCB_blockStart ;; store at DCB struct
    lda #<readHeaderDCB
    jsr loadDCB_execSIO ;; second word (blockEnd)

    inw tmp_address ;; calculate data length
    sec               
    lda tmp_address
    sbc DCB_blockStart
    sta DCB_blockLen        ;; and store in DCB.DBYT
    sta DCB_blockLen + 2    ;; and DCB.AUX
    lda tmp_address + 1
    sbc DCB_blockStart + 1
    sta DCB_blockLen + 1
    sta DCB_blockLen + 3

getBlock
    lda #<readDataDCB
    jsr loadDCB_execSIO

endOfData
    lda DCB_blockStart + 1
    cmp #$02  ;; check if saved on $02 page
    bne endOfBlock
    lda DCB_blockStart
    cmp #$E0 ;; RUNAD modified
    sne 
    inc shouldRun + 1
    cmp #$E2 ;; INITAD modified
    bne endOfBlock
    mwa $02E2 initjsr+1
initjsr    
    jsr $FFFF ;; <- jump address gets replaced by command above

endOfBlock
    jsr checkForData
    jne blockLoop
                        ;; close connection
closeConnection
    lda #<closeConnectionDCB
    jsr loadDCB_execSIO

shouldRun
    lda #0  ;; <- self modified code. byte value gets updated somewhere else
    beq clearEnd
    mwa $02E0 runjump+1
runjump
    jmp $FFFF ;; <- jump address gets replaced by command above

clearEnd
    ldy #1

error
    ;; errorcode in Y register
    rts

;; DCB structs  

openConnectionDCB
    dta $71,1,'O',_W
connection_string_address
        dta a(0),TCP_TIMEOUT,0,a(256)
open_aux1
        dta METHOD_GET
open_aux2
        dta 0 // no EOL translation

getStatusDCB
    dta $71,1,'S',_R,a(status),TCP_TIMEOUT,0,a(4),0,0 

readDataDCB
    dta $71,1,'R',_R
DCB_blockStart
        dta a(0),TCP_TIMEOUT,0
DCB_blockLen 
        dta a(0),a(0) 

readHeaderDCB
    dta $71,1,'R',_R,a(tmp_address),TCP_TIMEOUT,0,a(2),a(2) 

closeConnectionDCB
    dta $71,1,'C',_NO,a(0),0,0,a(0),0,0   
structsEnd

checkForData
    lda #<getStatusDCB
    jsr loadDCB_execSIO
    lda status
    ora status + 1
    rts
    
    ;; copies struct to DCB
loadDCB_execSIO
    sta loadDCBloop + 1 ;; updates loop address
    ldy #11 
loadDCBloop 
    lda openConnectionDCB,y  ;; <- self modyfing code here
    sta DCB,y
    dey
    bpl loadDCBloop 
execSio
    jsr JSIOINT 
    rts

;;;;;;;;;;;;;;; VARS

status
    dta a(0)    ;; size
tmp_address
    dta a(0)
errorcode = tmp_address + 1


.IF [>structsEnd <> >openConnectionDCB] 
    .PRINT "structsEnd: ", structsEnd
    .ERROR "DBA structs splited on two memory pages!"
.ENDIF
   
.PRINT "LOADER_SIZE = ", *-loader, ";"
.PRINT "LOADER_VECTOR = ", loader, ";"
.PRINT "DEST_VECTOR = ", DCB_blockStart, ";"
.PRINT "URI_VECTOR = ", connection_string_address, ";"




