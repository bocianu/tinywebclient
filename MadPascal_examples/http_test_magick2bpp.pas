program http_test;
{$librarypath '..'} 
uses atari, http_client, graph, crt;

var
    i: byte;
    url: pChar = 'N:http://192.168.1.201:32124/gfx/convert';
    headers: pChar = 'Content-Type: application/json'#0#0;
    request1: string = '{"url":"https://i.pinimg.com/originals/11/eb/dc/11ebdcc531a218b1c9c5fc3711d6cd85.jpg","mode":"2bpp"}';
    request2: string = '{"url":"https://img.redro.pl/plakaty/mysterious-moon-in-dark-forest-at-night-scary-dark-landscape-700-195323245.jpg","mode":"2bpp"}';
(*
    request3: string = '{"path":"img/test.jpg","mode":"1bpp"}';
    request4: string = '{"path":"img/test3.jpg","mode":"2bpp","threshold":"85"}';
    request5: string = '{"url":"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Atari_logo_alt.svg/1200px-Atari_logo_alt.svg.png","mode":"1bpp"}';
    request6: string = '{"url":"https://fujinet.online/wp-content/uploads/2019/12/FujiNetLogoW.jpg","mode":"1bpp","background":"black"}';
    request7: string = '{"url":"https://media.istockphoto.com/vectors/desktop-computer-vector-id862739260","mode":"1bpp"}';
    request8: string = '{"url":"https://sklep.selected.pl/images/obrazki_przedmiotow/EATERS/PACMAN.png","mode":"1bpp"}';
    request9: string = '{"url":"https://media.istockphoto.com/vectors/abstract-vector-landscape-nature-or-outdoor-mountain-view-silhouette-vector-id1004093210","mode":"1bpp"}';
    request10: string = '{"url":"https://img.freepik.com/free-vector/vine-hills-landscape-sketch_507816-101.jpg","mode":"1bpp"}';
    request11: string = '{"url":"https://png.pngtree.com/png-clipart/20210928/ourlarge/pngtree-halloween-frame-haunted-house-silhouette-black-png-image_3954031.png","mode":"1bpp"}';
    request12: string = '{"url":"https://st2.depositphotos.com/8384334/11169/v/950/depositphotos_111690778-stock-illustration-cityscape-vector-illustration-line-sketched.jpg","mode":"1bpp"}';
*)
//	strAddr: array [0..11] of word = (@request1, @request2, @request3, @request4, @request5, @request6, @request7, @request8,@request9, @request10, @request11, @request12);
	strAddr: array [0..1] of word = (@request1, @request2); //, @request3, @request4, @request5, @request6, @request7, @request8,@request9, @request10, @request11, @request12);

begin
	InitGraph(15+16);
	color0 := 4;
	color1 := 8;
	color2 := 12;
	color3 := $34;
	repeat
		for i:=0 to 1 do begin
			Pause;
			HTTP_headers := @headers;
			HTTP_Post(@url, pointer(savmsc), pointer(strAddr[i]+1), peek(strAddr[i]));
			Readkey;
		end;
	until false;
end.
	
