program http_test_get_api;
{$librarypath '..'} 
uses http_client, crt;

var
    url: pChar = 'N:https://www.boredapi.com/api/activity';
    responseBuffer: array [0..0] of byte absolute $4000;
    key: char;
    s: string;

{$i json.inc}
    
begin
	repeat
		ClrScr;
		Writeln('www.boredapi.com'*);
		Writeln;

		HTTP_Get(@url, responseBuffer);
		if HTTP_error <> 1 then writeln('Error: ', HTTP_error);
		InitJson(HTTP_respSize);

		Writeln(GetJsonKeyValue('activity'));
		Writeln;
		Writeln('Activity type: ',GetJsonKeyValue('type'));
		Writeln;
		Writeln('Participants: ',GetJsonKeyValue('participants'));
		Writeln;
		Writeln('Price: ',GetJsonKeyValue('price'));
		Writeln;
		key := Readkey;
	until key = 'q';
	Writeln('bye bye...');
end.
