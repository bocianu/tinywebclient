const
{$i '../http_loader.inc'}

function HTTP_Get(url: pointer; addr: word):byte;
var 
    uriPtr: pointer absolute URI_VECTOR;
    dest: word absolute DEST_VECTOR;
begin
	uriPtr := url;
	dest := addr;
    asm 
		jsr LOADER_VECTOR 
		sty result
	end;
end;
